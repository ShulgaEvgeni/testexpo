const tintColor = '#f0b706'

export default {
  main: {
    tabBarBackgraund: '#141317',
    ////////////////////////////////////////////
    headerBackgraund: '#141317',
    headerLabel: '#FFFFFF',
    ////////////////////////////////////////////
    inactivButton: '#737382',
    ////////////////////////////////////////////
    text: '#FFFFFF',
    background: '#000000',
    ////////////////////////////////////////////
    tintColor,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColor,
    tabBar: '#fefefe',
    errorBackground: '#E63129',
    errorText: '#fff',
    warningBackground: '#eaeb5e',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
    accept: '#2aad1e',
    disable: '#9999A0',
    textComment: '#737382',
    primary: '#f0b706',
    profileText: '#fafafa',
    dark: '#141317',
    placeholder: '#0f0',
    border: '#eaeaea',
    // header: 'rgba(20, 19, 23, 0.7)'
    header: '#141317',
    chevronDown: '#424047'
  }
};
