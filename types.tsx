import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'

export type StackNavigationProps = {
  WelcomeFirst: undefined
  WelcomeSecond: undefined
  WelcomeThird: undefined
  Main: undefined
  QrScreen: undefined
  NotFound: undefined
  PurseScreen: undefined
  EditMyProfile: undefined
}

export type RootStackParamList = {
  Welcome: undefined
  Main: undefined
  NotFound: undefined
  Second: undefined
  QrScreen: undefined
}

export type BottomTabParamList = {
  Network: undefined
  Profile: undefined
  Shops: undefined
}

export type TabOneParamList = {
  TabOneScreen: undefined
  Second: undefined
}

export type TabTwoParamList = {
  TabTwoScreen: undefined
}

export type TabThreeParamList ={
  TabThreeScreen: undefined
  EditMyProfile: undefined
}

export type IWelcomeStack = {
  WelcomeFirst: undefined
  WelcomeSecond: undefined
  WelcomeThird: undefined
}

export type ProfileScreenNavigationProp = StackNavigationProp<StackNavigationProps>

export type Props = {
  navigation: ProfileScreenNavigationProp;
}
