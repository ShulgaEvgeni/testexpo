import React, { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import {
  StyleSheet
} from 'react-native'

import useCachedResources from './hooks/useCachedResources';
import Navigation from './navigation';
import { FIRST_LOGIN } from './localStorage/value'
import { retrieveData } from './localStorage'

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: "column",
    justifyContent: 'center',
    backgroundColor: '#000000a0'
  }
})

export default function App() {
  const isLoadingComplete = useCachedResources()
  //////////////////////////////////////////////////////////
  const [getFisrt, setGetFisrt] = useState(false)
  const [first, setFirst] = useState(false)
  //////////////////////////////////////////////////////////

  const checkFirstLogin = async () => {
    const a = await retrieveData(FIRST_LOGIN)
    if (a === 'true') {
      setGetFisrt(true)
      return true
    } else {
      setFirst(true)
      setGetFisrt(true)
      return false
    }
  }

  useEffect(() => {
    checkFirstLogin()
  }, [])

  if (!isLoadingComplete && getFisrt) {
    return null;
  } else {
    return (
      <SafeAreaProvider style={styles.container}>
        <Navigation colorScheme={null} first={first} />
        <StatusBar style='light' hideTransitionAnimation='slide' />
      </SafeAreaProvider>
    );
  }
}
