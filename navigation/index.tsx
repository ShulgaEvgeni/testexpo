import * as React from 'react';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ColorSchemeName } from 'react-native';

import Colors from '../constants/Colors'
import NotFoundScreen from '../screens/NotFoundScreen';
import { QrScreen } from '../screens';
import { RootStackParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import WelcomeNavigator from './WelcomeStack';
import LinkingConfiguration from './LinkingConfiguration';

export default function Navigation({ colorScheme, first }: { colorScheme: ColorSchemeName, first?: boolean }) {
  const initRoute = first ? 'Welcome' : 'Main'
  const Stack = createStackNavigator<RootStackParamList>();

  const screenOptions = {
    headerShown: true,
    headerTintColor: Colors.main.headerLabel,
    headerBackTitle: ' ',
    headerStyle: {
      backgroundColor: Colors.main.headerBackgraund
    }
  }

  const RootNavigator = () => {
    return (
      <Stack.Navigator
        initialRouteName={initRoute}
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="Welcome" component={WelcomeNavigator} />
        <Stack.Screen name="Main" component={BottomTabNavigator} />
        <Stack.Screen options={screenOptions} name="QrScreen" component={QrScreen} />
        <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
      </Stack.Navigator>
    );
  }

  return (
    <NavigationContainer
      linking={LinkingConfiguration}
    >
      <RootNavigator />
    </NavigationContainer>
  );
}

