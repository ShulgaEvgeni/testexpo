import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { IWelcomeStack } from '../types'
import {
  WelcomeFirst,
  WelcomeSecond,
  FirstThree
} from '../screens'
import { Colors } from '../constants'

const WelcomeStack = createStackNavigator<IWelcomeStack>();

export default function TabBarIcon(props: { name: string; color: string }) {

  const screenOptions = {
    headerShown: false
  }


  return (
    <WelcomeStack.Navigator
      initialRouteName='WelcomeFirst'
    >
      <WelcomeStack.Screen
        name='WelcomeFirst'
        component={WelcomeFirst}
        options={screenOptions}
      />
      <WelcomeStack.Screen
        name='WelcomeSecond'
        component={WelcomeSecond}
        options={screenOptions}
      />
      <WelcomeStack.Screen
        name='WelcomeThird'
        component={FirstThree}
        options={screenOptions}
      />
    </WelcomeStack.Navigator>
  )
}




