import * as React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator, HeaderBackButton, HeaderTitle } from '@react-navigation/stack'

import Colors from '../constants/Colors'
import {
  Shop,
  MyNetwork,
  MyProfile,
  EditMyProfile
} from '../screens'
import {
  BottomTabParamList,
  TabOneParamList,
  TabTwoParamList,
  TabThreeParamList
} from '../types'
import {
  ShopIcon,
  NetworkIcon,
  ProfileIcon
} from '../assets/icons'

const BottomTab = createBottomTabNavigator<BottomTabParamList>()

const screenOptions = {
  headerTintColor: Colors.main.headerLabel,
  headerBackTitle: ' ',
  headerStyle: {
    backgroundColor: Colors.main.headerBackgraund
  }
}

const tabBarOptions = {
  activeTintColor: Colors.main.tabIconSelected,
  inactiveTintColor: Colors.main.tabIconDefault,
  showLabel: false,
  style: {
    backgroundColor: Colors.main.tabBarBackgraund,
    borderTopColor: 'transparent'
  }
}

export default function BottomTabNavigator() {

  return (
    <BottomTab.Navigator
      initialRouteName='Shops'
      tabBarOptions={tabBarOptions}
    >
      <BottomTab.Screen
        name='Shops'
        component={TabOneNavigator}
        options={{
          tabBarIcon: ({ color }) => <ShopIcon fill={color} />
        }}
      />
      <BottomTab.Screen
        name='Network'
        component={TabTwoNavigator}
        options={{
          tabBarIcon: ({ color }) => <NetworkIcon fill={color} />
        }}
      />
      <BottomTab.Screen
        name='Profile'
        component={TabThreeNavigator}
        options={{
          tabBarIcon: ({ color }) => <ProfileIcon fill={color} />
        }}
      />
    </BottomTab.Navigator>
  );
}
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name='TabOneScreen'
        component={Shop}
        options={screenOptions}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name='TabTwoScreen'
        component={MyNetwork}
        options={screenOptions}
      />
    </TabTwoStack.Navigator>
  );
}

const TabThreeStack = createStackNavigator<TabThreeParamList>();

function TabThreeNavigator() {
  return (
    <TabThreeStack.Navigator>
      <TabThreeStack.Screen
        name='TabThreeScreen'
        component={MyProfile}
        options={screenOptions}
      />
      <TabThreeStack.Screen
        name='EditMyProfile'
        component={EditMyProfile}
        options={screenOptions}
      />
    </TabThreeStack.Navigator>
  );
}