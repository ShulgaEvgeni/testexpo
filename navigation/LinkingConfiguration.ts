import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Welcome: {
        screens: {
          WelcomeFirst: 'WelcomeFirst',
          WelcomeSecond: 'WelcomeSecond',
          WelcomeThird: 'WelcomeThird'
        }
      },
      Main: {
        screens: {
          TabOne: {
            screens: {
              TabOneScreen: 'one',
            },
          },
          TabTwo: {
            screens: {
              TabTwoScreen: 'two',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
