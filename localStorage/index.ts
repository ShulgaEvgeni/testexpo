import { AsyncStorage } from 'react-native'

export const storeData = async (
  name: string,
  value: string
) => {
  try {
    await AsyncStorage.setItem(name, value)
    return true
  } catch (error) {
    return false
  }
}

export const retrieveData = async (
  name: string
) => {
  try {
    const value = await AsyncStorage.getItem(name)
    if (value !== null) {
      return value
    }
  } catch (error) {
    return undefined
  }
}
