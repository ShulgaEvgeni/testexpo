import { EnvFunction } from '@babel/core'
import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableOpacity
} from 'react-native'

import { Card } from '../components'
import { Colors, Layout } from '../constants'

type IdataItem = {
  id: string,
  onPress?: Function,
  img: string,
  name?: string,
  title?: string
}

type Iprops = {
  style?: object,
  smallText?: string,
  onPress?: EnvFunction,
  color?: string,
  more?: string,
  title: string,
  special?: boolean,
  data: IdataItem[] | []
}


const styles = StyleSheet.create({
  horizontalScroll: {
    marginTop: Layout.window.height * 0.01,
    marginBottom: Layout.window.height * 0.01,
    maxWidth: '100%'
  },
  head: {
    paddingHorizontal: Layout.window.width * 0.04,
    flexDirection: 'row',
    marginBottom: 15,
    justifyContent: 'space-between',
    alignContent: 'flex-end'
  },
  moreText: {
    fontSize: 14,
    fontWeight: '800'
  },
  logoText: {
    fontSize: 18,
    fontWeight: '800',
    color: Colors.main.noticeText
  },
  smallText: {
    fontSize: 14,
    color: Colors.main.textComment
  },
  padding: {
    width: Layout.window.width * 0.03,
    backgroundColor: 'gold'
  }
})

export default (props: Iprops) => (
  <View style={{ ...styles.horizontalScroll, ...props.style }}>
    <View style={styles.head}>
      <Text style={props.smallText ? styles.smallText : styles.logoText}>{props.title}</Text>
      <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}>
        <Text style={{ ...styles.moreText, color: props.color }}>{props.more}</Text>
      </TouchableOpacity>
    </View>
    <FlatList
      data={props.data}
      renderItem={({ item }) => <Card special={!!props.special} data={item} />}
      keyExtractor={(item, index) => `${index}`}
      horizontal
      overScrollMode='never'
      showsHorizontalScrollIndicator={false}
      alwaysBounceHorizontal={false}
      ListHeaderComponent={() => <View style={styles.padding} />}
      ListFooterComponent={() => <View style={styles.padding} />}
    />
  </View>
)
