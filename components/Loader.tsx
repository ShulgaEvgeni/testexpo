import React from 'react'
import {
    View,
    StyleSheet,
    ActivityIndicator
} from 'react-native'
import { Colors } from '../constants'

const styles = StyleSheet.create({
    panel: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%'
    },
    spiner: {
        color: 'white'
    }
})

export default () => (
    <View style={styles.panel}>
        <ActivityIndicator
            animating={true}
            hidesWhenStopped={true}
            size='large'
            color={Colors.main.border}
        />
    </View>
)
