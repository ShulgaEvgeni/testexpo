import React from 'react'
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet
} from 'react-native'
import {
  Colors,
  Layout,
  Localisation as Data
} from '../constants'
import { getLocalisationId, retrieveData } from '../localStorage'
import { LOCALISATIONID } from '../localStorage/value'
import RNPickerSelect from 'react-native-picker-select'
import { Chevron } from 'react-native-shapes'

const scale = Layout.window.height > 600 ? 1.5 : 1.3

const styles = StyleSheet.create({
  languageBlock: {
    backgroundColor: Colors.dark,
    height: 30,
    width: 54,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    scaleX: scale,
    scaleY: scale,
    marginTop: 20,
    position: 'relative'
  },
  inputIOS: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    height: 30,
    width: 54,
    paddingLeft: 5,
    color: Colors.primary,
    zIndex: 10
  },
  inputAndroid: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    height: 30,
    width: 54,
    paddingLeft: 5,
    color: Colors.primary,
    zIndex: 10
  },
  iconContainer: {
    top: 10,
    right: 10
  }
})

export default (props: any) => {
  // constructor(props) {
  //   super(props)
  //   this.state = {
  //     localisation: 'en',
  //     modalVisible: false,
  //     items: []
  //   }
  //   this.inputRefs = {}
  // }

  // async CDM() {
  //   const local = await retrieveData(LOCALISATIONID)
  //   await this.setState({ localisation: local })
  // }

  // componentDidMount() {
  //   this.CDM()
  //   this.changeItem()
  // }

  // changeItem() {
  //   var items = []
  //   for (var k in Data) {
  //     items = items.concat({
  //       label: Data[k].title,
  //       value: Data[k].id.toUpperCase(),
  //       displayValue: true
  //     })
  //   }
  //   this.setState({ items })
  // }

  render() {
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => { this.setState({ modalVisible: true }) }}
        >
          <View style={styles.languageBlock}>
            <RNPickerSelect
              placeholder={{
                label: 'Select a language',
                value: null
              }}
              items={this.state.items}
              onValueChange={async (value) => {
                if (value === null) { value = this.state.items[0].value }
                await this.setState({
                  localisation: value
                })
                await getLocalisationId(value.toLowerCase())
                this.props.changeContent()
              }}
              style={{ ...styles }}
              value={this.state.localisation.toUpperCase()}
              ref={(el) => {
                this.inputRefs.picker = el
              }}
              Icon={() => {
                return <Chevron size={1.5} color={Colors.chevronDown} />
              }}
              useNativeAndroidPickerStyle={false}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }
}
