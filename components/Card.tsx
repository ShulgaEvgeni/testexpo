import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground
} from 'react-native'

import { Colors, Layout } from '../constants'

import defAva from '../assets/images/defAva.jpg'

type Iprops = {
  data: {
    id: string,
    onPress?: Function,
    img: string,
    name?: string,
    title?: string
  },
  table?: boolean,
  special: boolean
}

const styles = StyleSheet.create({
  textComment: {
    textAlign: 'center',
    fontSize: 14,
    marginBottom: Layout.window.height * 0.01
  },
  textName: {
    color: Colors.main.noticeText,
    textAlign: 'center',
    fontSize: 16,
    marginTop: Layout.window.height * 0.01,
    fontWeight: '600'
  },
  imageContainer: {
    width: '100%',
    height: Layout.window.width * 0.19,
    borderRadius: 15,
    overflow: 'hidden',
    backgroundColor: Colors.main.dark
  },
  img: {
    width: '100%',
    height: '100%'
  },
  card: {
    marginHorizontal: Layout.window.width * 0.01,
    width: Layout.window.width * 0.21,
    flex: 1
  },
  cardList: {
    marginHorizontal: Layout.window.width * 0.01,
    maxWidth: '23%',
    flex: 1
  }
})

export default (props: Iprops) => {
  const onPressCard = () => {
    if (!!props.data.onPress)
      props.data.onPress()
  }
  return (
    <View style={ props.table ? styles.cardList : styles.card}>
      <TouchableOpacity activeOpacity={0.8} onPress={onPressCard}>
        <ImageBackground source={defAva} style={styles.imageContainer}>
          <Image
            source={{ uri: props.data.img }}
            style={styles.img}
          />
        </ImageBackground>
      </TouchableOpacity>
      <View>
        {
          props.data.name
            ? <Text style={styles.textName}>{props.data.name}</Text>
            : <View />
        }
        <Text
          style={{
            ...styles.textComment,
            color: props.special ? Colors.main.accept : Colors.main.textComment
          }}
        >
          {props.data.title}
        </Text>
      </View>
    </View>
  )
}
