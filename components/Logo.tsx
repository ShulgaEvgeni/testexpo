import React from 'react'
import {
  View,
  Image,
  StyleSheet
} from 'react-native'

import { Layout } from '../constants'
import logo1 from '../assets/images/hellow-screen-decor1.png'
import logo8 from '../assets/images/hellow-screen-decor2.png'
import Earth from '../assets/images/earth.gif'

const styles = StyleSheet.create({
  imageContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  img: {
    width: 47
  },
  img1: {
    position: 'relative',
    left: Layout.window.width * 0.025
  },
  img8: {
    position: 'relative',
    right: Layout.window.width * 0.11
  },
  earthContainer: {
    width: Layout.window.width * 0.6,
    height: Layout.window.width * 0.6,
    borderRadius: Layout.window.width * 0.3
  },
  earth: {
    width: '100%',
    height: '100%'
  },
  mainblock: {
    position: 'relative'
  },
  children: {
    position: 'absolute',
    top: Layout.window.width * 0.45
  }
})

type props = {
  style?: any
  children?: any
  left?: string | number
  top?: string | number
}

export default (props: props) => (
  <View style={styles.mainblock}>
    <View style={{ ...styles.imageContainer, ...props.style }}>
      <View style={{ ...styles.img, ...styles.img1 }}>
        <Image
          source={logo1}
          resizeMode='contain'
        />
      </View>
      <View
        style={{
          ...styles.earthContainer,
          left: props.left,
          top: props.top
        }}
      >
        <Image
          source={Earth}
          style={styles.earth}
        />
      </View>
      <View style={{ ...styles.img, ...styles.img8 }}>
        <Image
          source={logo8}
          resizeMode='contain'
        />
      </View>
    </View>
    <View style={styles.children}>
      {props.children}
    </View>
  </View>
)
