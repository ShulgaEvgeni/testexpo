import React from 'react'
import { View, Image, StyleSheet } from 'react-native'

import { Layout } from '../constants'

import Earth from '../assets/images/earth.gif'

type IEarth = {
  absolute?: boolean,
  left?: Number,
  top?: Number
}

const styles = StyleSheet.create({
  imageContainer: {
    width: Layout.window.width * 0.6,
    height: Layout.window.width * 0.6,
    borderRadius: Layout.window.width * 0.3,
    overflow: 'hidden',
    position: 'relative'
  },
  earth: {
    width: '100%',
    height: '100%'
  }
})

export default (props: IEarth) => {
  let abs = {
    Pos: {},
    Left: {},
    Top: {}
  }
  if (props.absolute) {
    abs.Pos = {
      position: 'absolute'
    }
  }
  if (props.left) {
    abs.Left = {
      left: props.left
    }
  }
  if (props.top) {
    abs.Top = {
      top: props.top
    }
  }
  const dopStyles = StyleSheet.create(abs)
  return (
    <View
      style={{
        ...styles.imageContainer,
        ...dopStyles.Pos,
        ...dopStyles.Left,
        ...dopStyles.Top,
      }}
    >
      <Image
        source={Earth}
        style={styles.earth}
      />
    </View>
  )
}
