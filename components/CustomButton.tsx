import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import { Colors, Layout } from '../constants'

type props = {
  children: any
  color: string
  onPress: any
  boxShadow?: true | boolean
  border?: true | boolean
  whisoutBorder?: true | boolean
  disabled?: true | boolean
  customStyle?: object
}

const styles = StyleSheet.create({
  buttonText: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#FFF'
  },
  loginButton: {
    height: Layout.window.height * 0.06,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnContainer: {
    width: '100%'
  }
})

export default (props: props) => {
  let color = props.color

  if (props.disabled) {
    color = Colors.main.inactivButton
  }


  var border: any = {}
  var button: any = {}
  var text: any = {}

  if (props.boxShadow) {
    border = {
      ...border,
      shadowColor: color,
      shadowOffset: {
        width: 0,
        height: 0
      },
      shadowOpacity: 1,
      shadowRadius: 10.00,
      elevation: 25
    }
  }
  if (props.border) {
    button = {
      ...button,
      borderColor: color,
      borderWidth: 2,
      borderRadius: 7,
      backgroundColor: 'transparent'
    }
    text = {
      ...text,
      color: color
    }
  }
  if (props.whisoutBorder) {
    button = {
      ...button,
      backgroundColor: 'transparent',
    }
    text = {
      ...text,
      color: color
    }
  }



  const shadow = StyleSheet.create({
    Border: border,
    Button: button,
    Text: text
  })

  // if (props.icon) {
  //   return (
  //     <TouchableOpacity
  //       activeOpacity={0.8}
  //       onPress={props.onPress} style={{
  //         ...styles.btnContainer,
  //         ...shadow.loginButton,
  //         ...props.customStyle
  //       }}
  //     >
  //       <View style={{
  //         ...styles.loginButton,
  //         backgroundColor: color,
  //         ...shadow.backgroundColor,
  //         height: props.height
  //       }}
  //       >
  //         {props.children}
  //       </View>
  //     </TouchableOpacity>
  //   )
  // } else {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={props.onPress}
      style={{
        ...styles.btnContainer,
        ...shadow.Border,
        ...props.customStyle

      }}
      disabled={props.disabled}
    >
      <View style={{
        ...styles.loginButton,
        backgroundColor: color,
        ...shadow.Button
      }}
      >
        <Text style={{ ...styles.buttonText, ...shadow.Text }}>{props.children}</Text>
      </View>
    </TouchableOpacity>
  )
  // }
}
