import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Layout } from '../constants'

const borderWidth = 5
const styles = StyleSheet.create({
  imageContainer: {
    width: Layout.window.width * 0.5,
    height: Layout.window.width * 0.5,
    borderColor: 'white',
    overflow: 'hidden',
    marginHorizontal: Layout.window.width * 0.2,
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
    marginBottom: Layout.window.height * 0.05
  },
  border: {
    width: '15%',
    borderColor: 'black'
  },

  borderTopLeft: {
    borderTopWidth: borderWidth,
    borderLeftWidth: borderWidth
  },
  borderTopRight: {
    borderTopWidth: borderWidth,
    borderRightWidth: borderWidth
  },
  borderBottomLeft: {
    borderBottomWidth: borderWidth,
    borderLeftWidth: borderWidth
  },
  borderBottomRight: {
    borderBottomWidth: borderWidth,
    borderRightWidth: borderWidth
  },
  borderContainer: {
    height: '15%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  borderContainerTop: {
  },
  borderContainerBottom: {
    position: 'relative',
    bottom: '15%'
  },
  children: {
    width: '85%',
    height: '85%',
    marginHorizontal: '7.5%',
    position: 'relative',
    bottom: '7.5%'
  }
})

type Iprops = {
    children?: JSX.Element
}

export default (props: Iprops) => (
  <View style={styles.imageContainer}>
    <View style={{ ...styles.borderContainer, ...styles.borderContainerTop }}>
      <View style={{ ...styles.borderTopLeft, ...styles.border }} />
      <View style={{ ...styles.borderTopRight, ...styles.border }} />
    </View>
    <View style={styles.children}>
      {props.children}
    </View>
    <View style={{ ...styles.borderContainer, ...styles.borderContainerBottom }}>
      <View style={{ ...styles.borderBottomLeft, ...styles.border }} />
      <View style={{ ...styles.borderBottomRight, ...styles.border }} />
    </View>

  </View>
)