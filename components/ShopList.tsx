import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  FlatList
} from 'react-native'

import { Card } from '../components'
import { Colors, Layout } from '../constants'

type IdataItem = {
  id: string,
  onPress?: Function,
  img: string,
  name?: string,
  title?: string
}

type Iprops = {
  title: string,
  special?: boolean,
  data: IdataItem[] | []
}

const styles = StyleSheet.create({
  shopList: {
    marginTop: Layout.window.height * 0.01,
    marginBottom: Layout.window.height * 0.01,
    maxWidth: '100%'
  },
  head: {
    paddingHorizontal: Layout.window.width * 0.04,
    flexDirection: 'row',
    marginBottom: 10,
    justifyContent: 'space-between',
    alignContent: 'flex-end'
  },
  logoText: {
    fontSize: 18,
    fontWeight: '800',
    color: Colors.main.noticeText
  }
})

export default (props: Iprops) => (
  <View style={styles.shopList}>
    <View style={styles.head}>
      <Text style={styles.logoText}>{props.title}</Text>
    </View>
    <View style={{ flex: 1, padding: Layout.window.width * 0.04 }}>
      {
        props.data.length ?
          <FlatList
            numColumns={Layout.isSmallDevice ? 3 : 4}
            data={props.data}
            renderItem={({ item }) => <Card table special={!!props.special} data={item} />}
            keyExtractor={item => item.id}
            overScrollMode='never'
          />
          :
          <Text style={{ color: '#FFFFFF' }}>Пусто</Text>
      }
    </View>
  </View>
)
