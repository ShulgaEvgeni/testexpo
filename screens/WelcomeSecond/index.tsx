import React from 'react'
import { Props } from '../../types'
import {
  View,
  StatusBar,
  Text,
  ImageBackground,
  SafeAreaView,
  Image
} from 'react-native'

import {
  CustomButton
} from '../../components'
import {
  Colors,
  Layout
} from '../../constants'
import stylesMain from '../styles'
import styles from './styles'

import Scheme108 from '../../assets/images/scheme108.png'
import Stars from '../../assets/images/backgraund.png'

export default function WelcomeSecond({ navigation }: Props) {
  return (
    <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
      <Image
        source={Scheme108}
        resizeMode='contain'
        style={styles.scheme108}
      />
      <View>
        <Text style={styles.bigLogoText}>
          В чем магия?
        </Text>
        <Text style={styles.logoText}>
          Все просто: ты получаешь кэшбек не только за себя, но и за всех, кого позовешь в 108 AllWorld, и даже за тех, кого пригласят они.
        </Text>
      </View>
      <View style={{ margin: Layout.window.width * 0.04, ...styles.Button }}>
        <CustomButton
          color={Colors.main.primary}
          onPress={() => navigation.navigate('WelcomeThird')}
          border
        >
          Далее
        </CustomButton>
      </View>
    </ImageBackground>
  )
}
