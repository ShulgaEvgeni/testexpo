import { StyleSheet } from 'react-native'
import {
  Colors,
  Layout
} from '../constants'

const width = Layout.window.width
const height = Layout.window.height

export default
  StyleSheet.create({
    page: {
      backgroundColor: Colors.main.background,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1
    },
    pageWithoutHeader: {
      backgroundColor: Colors.main.background,
      paddingTop: '5%',
      height: '100%',
      display: 'flex',
      alignItems: 'center'
    },
    content: {
      width: '94%'
    },
    text: {
      color: Colors.main.text
    },
    background: {
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center"
    },
    input: {
      height: 45,
      backgroundColor: 'white',
      paddingLeft: 15,
      borderRadius: 5
    }
  })
