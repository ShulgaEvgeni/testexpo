import React, { useState, useEffect } from 'react'
import {
  ImageBackground,
  ScrollView,
  View,
  RefreshControl
} from 'react-native'

import { Props } from '../../types'
import {
  Earth,
  CustomButton,
  HorizontalScroll,
  Loader
} from '../../components'
import {
  Layout,
  Colors
} from '../../constants'

import stylesMain from '../styles'
import styles from './styles'

import Stars from '../../assets/images/backgraund.png'


type IdataItem = {
  id: string,
  onPress?: Function,
  img: string,
  name?: string,
  title?: string
}

const emptyLine = (title: string, data?: any) => (<HorizontalScroll data={data} title={title}></HorizontalScroll>)

export default function Shop({ navigation }: Props) {
  const [refreshing, setRefreshing] = useState(false)
  const [signin, setSignin] = useState(true)
  const [firstLine, setFirstLine] = useState<JSX.Element>(emptyLine('Первая линия (пусто)'))
  const [secondLine, setSecondLine] = useState<JSX.Element>(emptyLine('Вторая линия (пусто)'))
  const [thirdLine, setThirdLine] = useState<JSX.Element>(emptyLine('Третья линия (пусто)'))

  const onRefresh = () => {
    setRefreshing(true)
    setTimeout(() => { setRefreshing(false) }, 1000)
  }

  useEffect(() => {
    navigation.setOptions({
      headerTitle: 'Ваша сеть'
    })
  }, [])

  return (
    <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
      <Earth
        left={Layout.window.width * 0.3}
        top={Layout.window.height * 0.7}
        absolute
      />
      {signin ?
        <ScrollView
          style={styles.screen}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          {firstLine}
          {secondLine}
          {thirdLine}
          <View style={{ margin: Layout.window.width * 0.04 }}>
            <CustomButton
              color={Colors.main.primary}
              onPress={() => navigation.navigate('QrScreen')}
            >
              Подключить нового пользователя
          </CustomButton>
          </View>
        </ScrollView>
        :
        <Loader />
      }
    </ImageBackground >
  )
}
