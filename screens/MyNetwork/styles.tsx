import { StyleSheet } from 'react-native'
import { Colors, Layout } from '../../constants'

export default
  StyleSheet.create({
    // imageBackground: {
    //   flex: 1,
    //   backgroundColor: 'black'
    // },
    screen: {
      display: 'flex',
      width: '100%',
    },
    searchR: {
      backgroundColor: 'white'
    },
    search: {
      backgroundColor: 'white',
      color: 'black'
    },
    searchContainer: {
      backgroundColor: undefined,
      borderBottomColor: 'transparent',
      borderTopColor: 'transparent',
    },
    // nameText: {
    //   color: Colors.main.noticeText,
    //   fontSize: 17,
    //   fontWeight: '500'
    // },
    // commentText: {
    //   color: Colors.main.textComment,
    //   fontSize: 14,
    //   fontWeight: '100'
    // },
    // textContainer: {
    //   marginVertical: Layout.window.height * 0.015
    // },
    // imageContainer: {
    //   width: Layout.window.width * 0.9,
    //   height: Layout.window.width * 0.5,
    //   borderRadius: 15,
    //   overflow: 'hidden',
    //   backgroundColor: 'gold',
    //   justifyContent: 'center'
    // },
    // img: {
    //   width: '100%',
    //   height: '100%'
    // },
    // profile: {
    //   padding: Layout.window.width * 0.05
    // },
    // profileInfo: {
    //   flexDirection: 'row',
    //   maxWidth: '100%',
    //   padding: Layout.window.width * 0.08,
    //   marginTop: -Layout.window.width * 0.25
    // }
  })
