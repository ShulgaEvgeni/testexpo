import React, { useState } from 'react'
import {
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Linking,
} from 'react-native'
import { Icon } from 'react-native-elements'

import { Props } from '../../types'
import { CustomButton, Logo } from '../../components'
import { Colors } from '../../constants'
import stylesMain from '../styles'
import styles from './styles'

import Stars from '../../assets/images/backgraund.png'

export default function WelcomeFirst({ navigation }: Props) {
  const [checkPalicy, setCheckPalicy] = useState(true)

  return (
    <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
      <View style={styles.language}>
        {/* <LanguageBlock changeContent={() => { this.changeContent() }} /> */}
      </View>
      <View style={{ ...stylesMain.content }}>
        <Logo />
        <Text style={{ ...stylesMain.text, ...styles.bigLogoText }}>Привет! Круто, теперь ты с нами!</Text>
        <Text style={{ ...stylesMain.text, ...styles.logoText }}>108 AllWorld это система, в которой ты можешь получать кэшбек, превышающий стоимость твоих покупок.</Text>
        <CustomButton
          color='#F0B706'
          onPress={() => { navigation.navigate('WelcomeSecond') }}
          disabled={!checkPalicy}
          boxShadow
          border
        >
          Далее
          </CustomButton>
        <View style={styles.policyBlock}>
          <TouchableOpacity
            onPress={() => { setCheckPalicy(!checkPalicy) }}
          >
            <View style={styles.policyCheck}>
              <Icon
                name={checkPalicy ? 'check-circle' : 'checkbox-blank-circle-outline'}
                type='material-community'
                size={24}
                color={Colors.main.text}
              />
            </View>
          </TouchableOpacity>

          <Text style={styles.policyText}>
            Нажимая "Далее" я принимаю&nbsp;
              <Text
              style={styles.policyTextLink}
              onPress={() => { Linking.openURL('http://nonameteam.tilda.ws/108cashback') }}
            >
              политику конфиденциальности
              </Text>
          </Text>
        </View>
      </View>
    </ImageBackground>
  );
}
