import { StyleSheet } from 'react-native'
import {
  Colors,
  Layout
} from '../../constants'

const width = Layout.window.width
const height = Layout.window.height

export default
  StyleSheet.create({
    logoText: {
      fontSize: width * .040,
      marginHorizontal: width * 0.01,
      textAlign: 'center',
      marginBottom: height * .05,
    },
    bigLogoText: {
      fontSize: width * .077,
      fontWeight: 'bold',
      textAlign: 'center',
      marginHorizontal: width * 0.090,
      marginBottom: height * .05,
      marginTop: height * .098
    },

    language: {
      width: '100%',
      paddingRight: 20,
      alignItems: 'flex-end'
    },
    languageBlock: {
      height: 40,
      width: 50,
      borderRadius: 5,
      borderWidth: 2,
      justifyContent: 'center',
      alignItems: 'center'
    },
    languageBlockText: {
      color: 'white',
      fontWeight: 'bold'
    },
    languageContainerModal: {
      backgroundColor: '#000000',
      height: '100%',
      paddingTop: '10%'
    },
    languageContainerItem: {
      flexDirection: 'row',
      alignItems: 'center',
      marginLeft: '5%'
    },
    languageContainer: {
      width: '100%',
      height: 35
    },
    languageText: {
      marginRight: 20,
      marginLeft: '2%',
      fontSize:  width * .03,
      color: '#FFFFFF'
    },

    policyBlock: {
      width: '100%',
      flexDirection: 'row',
      marginTop: Layout.window.width * 0.07,
      justifyContent: 'center',
      alignItems: 'center'
    },
    policyCheck: {
      marginRight: width * 0.02
    },
    policyText: {
      color: Colors.main.text,
      fontSize: width * .03
    },
    policyTextLink: {
      color: Colors.main.text,
      textDecorationLine: 'underline'
    }
  })
