import React, { useState, useEffect } from 'react'
import {
    View,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    StatusBar,
    SafeAreaView,
    Share,
    TouchableWithoutFeedback
} from 'react-native'
import { Icon } from 'react-native-elements'
import Clipboard from 'expo-clipboard'
import QRCode from 'react-native-qrcode-svg';

import { Props } from '../../types'
import {
    Earth,
    CustomButton,
    QrBorder
} from '../../components'
import {
    Layout,
    Colors
} from '../../constants'

import stylesMain from '../styles'
import styles from './styles'

import Stars from '../../assets/images/backgraund.png'

const onShare = async (url: string) => {
    try {
        const result = await Share.share({
            message: url
        })

        if (result.action === Share.sharedAction) {
            if (result.activityType) {
                // shared with activity type of result.activityType
            } else {
                // shared
            }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
    } catch (error) {
        alert(error.message)
    }
}


export default function Shop({ navigation }: Props) {
    const [code, setCode] = useState('asdqdas2e')
    const [check, setCheck] = useState('content-copy')


    useEffect(() => {
        navigation.setOptions({
            headerTitle: ''
        })
    }, [])

    const copyCode = (code: string) => {
        Clipboard.setString(code)
        setCheck('check')
        setTimeout(() => {
            setCheck('content-copy')
        }, 500)
    }

    return (
        <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
            <Earth
                left={-Layout.window.width * 0.3}
                top={Layout.window.height * 0.8}
                absolute
            />
            <ScrollView style={styles.screen}>
                <View style={styles.qr}>
                    <Text style={styles.logoText}>Подключить нового пользователя</Text>
                    <View style={styles.blure}>
                        <QrBorder>
                            <QRCode
                                size={167}
                                value="http://awesome.link.qr"
                            />
                        </QrBorder>
                    </View>
                    <TouchableWithoutFeedback onPress={() => { copyCode('qweqweqwe') }}>
                        <View style={styles.codeContener}>
                            <View style={styles.codeContenerLeft} />
                            <View style={styles.codeContenerInfo}>
                                <Text style={styles.codeTitle}>Ваш код</Text>
                                <Text style={styles.code}>{code}</Text>
                            </View>
                            <View>
                                <Icon
                                    name={check} /// check | content-copy
                                    type='material-community'
                                    size={32}
                                    color='white'
                                />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    <CustomButton color={Colors.main.accept} onPress={() => { onShare('qwe') }}>Поделиться ссылкой</CustomButton>
                </View>
            </ScrollView>
        </ImageBackground >
    )
}
