import { StyleSheet } from 'react-native'
import { Colors, Layout } from '../../constants'

export default
    StyleSheet.create({
        screen: {
            display: 'flex',
            width: '100%',
        },
        imageBackground: {
            flex: 1,
            backgroundColor: 'black'
        },
        blure: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(255, 255, 255, 0.2)',
            paddingTop: 40,
            paddingBottom: 10,
            borderRadius: 10
        },
        screenContainer: {
            position: 'relative',
            flex: 1,
            marginTop: Layout.OS === 'ios' ? 45 : 75,
            paddingTop: Layout.OS === 'ios' ? 0 : 10
        },
        logoText: {
            color: Colors.main.noticeText,
            fontSize: 22,
            fontWeight: '500',
            width: Layout.window.width * 0.8,
            marginHorizontal: Layout.window.width * 0.05,
            textAlign: 'center',
            marginVertical: Layout.window.height * 0.05
        },
        img: {
            width: '100%',
            height: '100%'
        },
        qr: {
            padding: Layout.window.width * 0.05,
            justifyContent: 'space-around'
        },
        codeContener: {
            marginTop: 50,
            marginBottom: 20,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingVertical: 16,
            paddingHorizontal: 20,
            backgroundColor: Colors.main.dark,
            borderRadius: 12
        },
        codeContenerLeft: {
            width: 32
        },
        codeContenerInfo: {
            marginRight: 10
        },
        codeTitle: {
            color: 'white',
            fontSize: 22,
            fontWeight: '500',
            textAlign: 'center'
        },
        code: {
            color: 'white',
            fontSize: 18,
            fontWeight: '500',
            textAlign: 'center'
        }
    })
