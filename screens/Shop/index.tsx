import React, { useState, useEffect } from 'react'
import {
  ImageBackground,
  ScrollView,
  View,
  Text
} from 'react-native'
import { SearchBar } from 'react-native-elements'

import { Props } from '../../types'
import {
  Earth,
  ShopList,
  HorizontalScroll
} from '../../components'
import {
  Layout
} from '../../constants'

import stylesMain from '../styles'
import styles from './styles'

import Stars from '../../assets/images/backgraund.png'
import Data from './data.json'


type IdataItem = {
  id: string,
  onPress?: Function,
  img: string,
  name?: string,
  title?: string
}

export default function Shop({ navigation }: Props) {
  const [search, setSearch] = useState('')
  const [content, setContent] = useState(null)
  const [shops, setShops] = useState<IdataItem[] | []>([])
  const [shopsDefault, setShopsDefault] = useState<IdataItem[] | []>([])
  const [shopsStar, setShopsStar] = useState<IdataItem[] | []>([])

  useEffect(() => {
    navigation.setOptions({
      headerTitle: 'Магазины'
    })
    setShops(Data.shop)
    setShopsDefault(Data.shop)
    setShopsStar(Data.shopStar)
  }, [])
  useEffect(() => {
    const tempShops = shopsDefault.filter(item => {
      if (item.name?.toUpperCase()?.indexOf(search.toUpperCase()) !== -1) {
        return item
      }
    })
    setShops(tempShops)
  }, [search])

  return (
    <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
      <Earth
        left={Layout.window.width * 0.7}
        top={Layout.window.height * -0.05}
        absolute
      />
      <ScrollView style={styles.screen}>
        <SearchBar
          placeholder={'Поиск'}
          lightTheme
          round
          value={search}
          onChangeText={(ev: string) => { setSearch(ev) }}
          containerStyle={styles.searchContainer}
          inputStyle={styles.search}
          inputContainerStyle={styles.searchR}
        />
        {
          (shopsStar.length && search.length === 0) ?
            <HorizontalScroll
              data={shopsStar}
              title='Лучшие предложения'
              special
            /> :
            <View>
              {/* <Text style={{ color: '#FFFFFF' }}>Список пуст</Text> */}
            </View>
        }
        {
          <ShopList data={shops} title={`${search.length ? 'Результат поиска' : 'Все магазины'}`} />
        }
      </ScrollView>
    </ImageBackground >
  )
}
