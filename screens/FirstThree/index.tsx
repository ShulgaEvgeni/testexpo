import React from 'react'
import {
  View,
  StatusBar,
  Text,
  ImageBackground,
  SafeAreaView
} from 'react-native'

import { Props } from '../../types'
import {
  CustomButton,
  Earth
} from '../../components'
import {
  Colors,
  Layout
} from '../../constants'

import stylesMain from '../styles'
import styles from './styles'
import { FIRST_LOGIN } from '../../localStorage/value'
import { storeData } from '../../localStorage'

import Stars from '../../assets/images/backgraund.png'

export default function FirstThree({ navigation }: Props) {

  const end = () => {
    storeData(FIRST_LOGIN, 'true')
    navigation.replace('Main')
  }

  return (
    <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
      <View style={styles.earth}>
        <Earth />
      </View>
      <View>
        <Text style={styles.bigTitleText}>
          Start
        </Text>
        <Text style={styles.bigLogoText}>
          Начнем?
        </Text>
        <Text style={styles.logoText}>
          Заполняй профиль, приглашай друзей и отправляйся за покупками в один из 130 магазинов!
        </Text>
      </View>
      <View style={{ margin: Layout.window.width * 0.04, ...styles.Button }}>
        <CustomButton
          color={Colors.main.primary}
          onPress={() => { end() }}
          border
        >
          Начать
        </CustomButton>
      </View>
    </ImageBackground>
  )
}
