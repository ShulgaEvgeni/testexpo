import { StyleSheet } from 'react-native'
import {
  Colors,
  Layout
} from '../../constants'

const width = Layout.window.width
const height = Layout.window.height

export default
  StyleSheet.create({
    imageBackground: {
      flex: 1,
      backgroundColor: 'black'
    },
    screen: {
      flex: 1,
      marginTop: height * 0.05,
      justifyContent: 'space-evenly'
    },
    logoText: {
      color: Colors.main.noticeText,
      fontSize: 15,
      fontWeight: '400',
      width: width * 0.7,
      marginHorizontal: width * 0.15,
      textAlign: 'center',
      marginBottom: height * 0.04
    },
    earth: {
      width: '100%',
      alignItems: 'center',
      marginTop: height > 600 ? height * 0.1 : height * 0.05
    },
    bigTitleText: {
      color: Colors.main.primary,
      fontSize: 70,
      fontWeight: 'bold',
      textAlign: 'center',
      marginHorizontal: width * 0.15,
      marginBottom: 20,
      marginTop: height < 600 ? height * 0.05 : height * 0.06
    },
    bigLogoText: {
      color: Colors.main.noticeText,
      fontSize: 30,
      fontWeight: 'bold',
      textAlign: 'center',
      marginHorizontal: width * 0.15,
      marginBottom: 20
    },
    textComment: {
      color: Colors.main.textComment,
      fontSize: 14,
      textAlign: 'center'
    },
    language: {
      width: '100%',
      paddingRight: 20,
      alignItems: 'flex-end'
    },
    languageBlock: {
      height: 40,
      width: 50,
      borderRadius: 5,
      borderWidth: 2,
      borderColor: Colors.main.primary,
      justifyContent: 'center',
      alignItems: 'center'
    },
    languageBlockText: {
      color: 'white',
      fontWeight: 'bold'
    },
    languageContainerModal: {
      backgroundColor: '#000000',
      height: '100%',
      paddingTop: '10%'
    },
    languageContainerItem: {
      flexDirection: 'row',
      alignItems: 'center',
      marginLeft: '5%'
    },
    languageContainer: {
      width: '100%',
      height: 35
    },
    languageText: {
      marginRight: 20,
      marginLeft: '2%',
      fontSize: 20,
      color: '#FFFFFF'
    },
    Button: {
      width: '90%'
    }
  })
