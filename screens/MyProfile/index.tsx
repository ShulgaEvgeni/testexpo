import React, { useState, useEffect } from 'react'
import {
    ImageBackground,
    ScrollView,
    View,
    RefreshControl,
    Text,
    Image
} from 'react-native'
import { Icon } from 'react-native-elements'

import { Props } from '../../types'
import {
    Earth,
    CustomButton,
    HorizontalScroll,
    Loader
} from '../../components'
import {
    Layout,
    Colors
} from '../../constants'

import stylesMain from '../styles'
import styles from './styles'

import Stars from '../../assets/images/backgraund.png'
import defAva from '../../assets/images/defAva.jpg'

import Data from '../Shop/data.json'



type IdataItem = {
    id: string,
    onPress?: Function,
    img: string,
    name?: string,
    title?: string
}

const emptyLine = (title: string, data?: any) => (<HorizontalScroll data={data} title={title}></HorizontalScroll>)

export default function Shop({ navigation }: Props) {
    const [refreshing, setRefreshing] = useState(false)
    const [signin, setSignin] = useState(true)
    const [shopsStar, setShopsStar] = useState<IdataItem[] | []>([])

    const onRefresh = () => {
        setRefreshing(true)
        setTimeout(() => { setRefreshing(false) }, 1000)
    }

    useEffect(() => {
        setShopsStar(Data.shopStar)
        navigation.setOptions({
            headerTitle: 'Профиль',
            headerRight: () => (<Icon
                name='settings-outline'
                type='material-community'
                containerStyle={styles.headerRightButton}
                size={32}
                onPress={() => { navigation.navigate('EditMyProfile') }}
                color='white'
            />)
        })
    }, [])
    { /*this.state.user.user !== undefined ? this.state.user.user.firstName + ' ' + this.state.user.user.secondName : '' */ }
    { /* this.state.balance.toFixed(2) + ' ' + getSymbolFromCurrency('USD') */ }
    return (
        <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
            <Earth
                left={Layout.window.width * 0.7}
                top={Layout.window.height * 0.7}
                absolute
            />
            {signin ?
                <ScrollView
                    style={styles.screen}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                >
                    <View style={styles.profileInfo}>
                        <ImageBackground source={defAva} style={styles.imageContainer}>
                            <Image
                                source={require('../../assets/images/defAva.jpg')}
                                style={styles.img}
                            />
                        </ImageBackground>
                        <View style={{ width: Layout.window.width * 0.5, ...styles.contenerUser }}>
                            <Text style={styles.nameText}>QWeqwe Qwqdqw</Text>
                            <Text style={styles.cashText}>10.65 $</Text>
                            <CustomButton
                                onPress={() => navigation.navigate('PurseScreen')}
                                color={Colors.main.accept}
                            >
                                Вывести деньги
                            </CustomButton>
                        </View>
                    </View>
                    {
                        shopsStar.length ?
                            <HorizontalScroll
                                data={shopsStar}
                                title='Лучшие предложения'
                                special
                            /> :
                            <View>
                                {/* <Text style={{ color: '#FFFFFF' }}>Список пуст</Text> */}
                            </View>
                    }
                    <View style={{ margin: Layout.window.width * 0.04 }}>
                        <CustomButton
                            color={Colors.main.primary}
                            boxShadow
                            onPress={() => navigation.navigate('QrScreen')}
                        >
                            Подключить нового пользователя
                        </CustomButton>
                    </View>
                </ScrollView>
                :
                <Loader />
            }
        </ImageBackground >
    )
}
