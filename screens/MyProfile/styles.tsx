import { StyleSheet } from 'react-native'
import { Colors, Layout } from '../../constants'

export default
    StyleSheet.create({
        imageBackground: {
            flex: 1,
            backgroundColor: 'black'
        },
        screen: {
            flex: 1,
            width: '100%'
        },
        screenContainer: {
            position: 'relative',
            flex: 1,
            marginTop: Layout.OS === 'ios' ? 45 : 75,
            paddingTop: Layout.OS === 'ios' ? 0 : 10
        },
        profileInfo: {
            flexDirection: 'row',
            maxWidth: '100%',
            padding: Layout.window.width * 0.03
        },
        nameText: {
            color: Colors.main.noticeText,
            fontSize: 16,
            fontWeight: '500'
        },
        cashText: {
            color: Colors.main.noticeText,
            fontSize: 16,
            fontWeight: '100'
        },
        profileText: {
            fontSize: 20,
            marginTop: 5,
            marginBottom: 5,
            textAlign: 'left',
            color: Colors.main.profileText
        },
        loginFormView: {
            flex: 1,
            paddingHorizontal: Layout.window.width * 0.04
        },
        link: {
            color: Colors.main.primary
        },
        imageContainer: {
            width: Layout.window.width * 0.3,
            height: Layout.window.width * 0.3,
            borderRadius: 15,
            overflow: 'hidden',
            marginRight: Layout.window.width * 0.08
        },
        img: {
            width: '100%',
            height: '100%'
        },
        editLink: {
            color: Colors.main.textComment,
            textDecorationLine: 'underline',
            fontSize: 14,
            fontWeight: '800'
        },
        contenerUser: {
            justifyContent: 'space-between'
        },
        headerRightButton: {
            paddingRight: 10
        }
    })
