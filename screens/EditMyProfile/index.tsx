import React, { useState, useEffect } from 'react'
import {
    View,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TextInput,
    Platform,
    TouchableWithoutFeedback
} from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import DateTimePicker from '@react-native-community/datetimepicker'

import { Props } from '../../types'
import {
    CustomButton
} from '../../components'
import {
    Layout,
    Colors
} from '../../constants'

import stylesMain from '../styles'
import styles from './styles'

import Stars from '../../assets/images/backgraund.png'
import defAva from '../../assets/images/defAva.jpg'

export default function Shop({ navigation }: Props) {
    const [fName, setFName] = useState('')
    const [sName, setSName] = useState('')
    const [uri, setUri] = useState('')
    const [date, setDate] = useState<Date>(new Date())
    const [show, setShow] = useState(false)

    useEffect(() => {
        navigation.setOptions({
            headerTitle: 'Редактирование профиля'
        })
    }, [])

    const getDate = () => {
        return (`${date.getDate() < 10 ? '0' : ''}${date.getDate()}.${date.getMonth() < 10 ? '0' : ''}${date.getMonth() + 1}.${date.getFullYear()}`)
    }

    const onChange = (event: Event, Date: Date | undefined) => {
        const currentDate = Date || date
        setShow(Platform.OS === 'ios')
        setDate(currentDate)
    }

    const pickImage = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1
        })
        if (!result.cancelled) {
            console.log('qwe', result.uri)
            setUri(result.uri)
            // this.setState({
            //     uri: result.uri,
            //     ava: result.uri,
            //     save: false
            // })
        }
    }

    const getPermissionAsync = async () => {
        if (Platform.OS === 'ios') {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!')
            } else {
                pickImage()
            }
        } else {
            pickImage()
        }
    }

    return (
        <ImageBackground source={Stars} style={{ ...stylesMain.background, ...stylesMain.page }}>
            <ScrollView style={styles.screen}>
                <ImageBackground source={defAva} style={styles.imgContener}>
                    <Image
                        source={{ uri }}
                        style={styles.img}
                    />
                </ImageBackground>
                <View style={styles.conrolContener}>
                    <CustomButton
                        color={Colors.main.primary}
                        onPress={getPermissionAsync}
                        boxShadow
                    >
                        Загрузить новое фото
                    </CustomButton>
                    <View style={{ ...styles.inputContener, marginTop: 25 }}>
                        <Text style={styles.inputContenerText}>
                            Имя
                        </Text>
                        <TextInput
                            placeholder={'fName'}
                            value={fName}
                            onChangeText={text => { setFName(text) }}
                            style={{ ...stylesMain.input, ...styles.input }}
                        />
                    </View>
                    <View style={{ ...styles.inputContener }}>
                        <Text style={styles.inputContenerText}>
                            Фамилия
                        </Text>
                        <TextInput
                            placeholder={'sName'}
                            value={sName}
                            onChangeText={text => { setSName(text) }}
                            style={{ ...stylesMain.input, ...styles.input }}
                        />
                    </View>
                    <View style={styles.inputContener}>
                        <Text style={styles.inputContenerText}>
                            День рождения
                        </Text>
                        <TouchableWithoutFeedback
                            onPress={() => { setShow(!show) }}
                        >
                            <View style={{ ...stylesMain.input, ...styles.input }}>
                                <Text style={{ color: '#FFFFFF' }}>{getDate()}</Text>
                                {show &&
                                    <DateTimePicker
                                        style={{
                                            width: '100%'
                                        }}
                                        mode='date'
                                        value={date}
                                        onChange={onChange}
                                        maximumDate={new Date()}
                                        minimumDate={new Date('1900-01-01T03:24:00')}
                                    />
                                }
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    {/* <View style={styles.inputContener}>
                            <Text style={styles.inputContenerText}>
                                Язык
                            </Text>
                            <RNPickerSelect
                                placeholder={{
                                    label: 'Select a language',
                                    value: null
                                }}
                                items={this.state.items}
                                onValueChange={async (value) => {
                                    if (value === null) { value = this.state.items[0].value }
                                    await this.setState({
                                        localisation: value
                                    })
                                    await getLocalisationId(value.toLowerCase())
                                    this.changeSave()
                                }}
                                style={{ ...styles }}
                                value={this.state.localisation.toUpperCase()}
                                ref={(el) => {
                                    this.inputRefs.picker = el
                                }}
                                Icon={() => {
                                    return <Chevron size={1.5} color={Colors.main.chevronDown} />
                                }}
                                useNativeAndroidPickerStyle={false}
                            />
                        </View> */}
                    {/* <LoginBar
                            ok={this.state.ok}
                            fb={this.state.fb}
                            go={this.state.go}
                            vk={this.state.vk}
                            apple={this.state.apple}
                            style={styles.LoginBar}
                        /> */}
                    <CustomButton
                        color={Colors.main.primary}
                        onPress={() => { }}
                    // border={this.state.save}
                    >
                        Сохранить
                        </CustomButton>
                    <CustomButton
                        color={Colors.main.errorBackground}
                        onPress={() => { }}
                        whisoutBorder
                    >
                        Выйти из профиля
                        </CustomButton>
                </View>
            </ScrollView>
        </ImageBackground >
    )
}
