import { StyleSheet } from 'react-native'
import { Colors, Layout } from '../../constants'

export default
    StyleSheet.create({
        imageBackground: {
            flex: 1,
            backgroundColor: 'black'
        },
        screen: {
            flex: 1,
            width: '100%'
        },
        imgContener: {
            width: '100%',
            height: Layout.window.height * 0.3
        },
        languageContainerModal: {
            backgroundColor: '#000000',
            height: '100%',
            paddingTop: '10%'
        },
        languageContainerItem: {
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: '5%'
        },
        languageContainer: {
            width: '100%',
            height: 35
        },
        languageText: {
            marginRight: 20,
            marginLeft: '2%',
            fontSize: 20,
            color: '#FFFFFF'
        },
        img: {
            width: '100%',
            height: '100%'
        },
        conrolContener: {
            flex: 1,
            paddingLeft: Layout.window.width * 0.05,
            paddingRight: Layout.window.width * 0.05,
            paddingBottom: Layout.window.width * 0.05,
            justifyContent: 'space-around',
            position: 'relative',
            bottom: 22
        },
        inputContener: {
            marginTop: 15
        },
        inputContenerText: {
            color: Colors.main.textComment,
            fontSize: 14
        },
        inputAl: {
            color: 'white',
            fontSize: 14
        },
        input: {
            width: '100%',
            marginTop: 8,
            justifyContent: 'center',
            borderWidth: 2,
            backgroundColor: Colors.main.dark,
            color: 'white'
        },
        LoginBar: {
            marginTop: Layout.window.height * 0.05,
            marginBottom: Layout.window.height * 0.02
        },
        inputIOS: {
            backgroundColor: Colors.main.dark,
            marginTop: 8,
            width: '100%',
            paddingLeft: 15,
            borderWidth: 2,
            color: 'white',
            borderRadius: 5,
            height: 45
        },
        inputAndroid: {
            backgroundColor: Colors.main.dark,
            marginTop: 8,
            width: '100%',
            paddingLeft: 15,
            borderWidth: 2,
            color: 'white',
            borderRadius: 5,
            height: 45
        },
        iconContainer: {
            top: 26,
            right: 15
        }
    })
